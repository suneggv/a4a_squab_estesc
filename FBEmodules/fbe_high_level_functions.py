from module_imports import *

# == imports -- mid ============================================================

# -- _______ -- ___ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import get_beta_now # beta_now(l)

# -- _______ -- ____ -- Analysis filter bank -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import AFB_sidechain # w_l^now for n^now

# -- _______ -- ____ -- Principal function -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import RT_plot # Real-time plotting


# == imports -- sunk ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import get_Y_now   # Y_i_now
from fbe_sunk_level_functions import update_Ymat # Y_mat, Y_1, Y_S, Y_L
from fbe_sunk_level_functions import get_W_now   # W_i_now
from fbe_sunk_level_functions import get_w_now   # w_l_now

# -- _______ -- ____ -- Analysis filter bank -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import update_stats # P_i,E(P_i),E(P_i^2),Var(P_i)


# == imports -- yawn ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_yawn_level_functions import get_P               # P_i
from fbe_yawn_level_functions import get_avgP            # E(P_i)
from fbe_yawn_level_functions import get_avgP2           # E(P_i^2)
from fbe_yawn_level_functions import get_varP            # Var(P_i)
from fbe_yawn_level_functions import spect_lin_to_dB_SPL # W_i_now
from fbe_yawn_level_functions import get_W_ABS           # W_i_now
from fbe_yawn_level_functions import get_W_FSh           # W_i_now



# ~~ 2.2 Import audio ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def import_audio( file_name,
                  the_file_is_stereo
                ): 
    if DEBUGGING:
        assert type(file_name)          == str
        assert      file_name[-4:]      == ".mp3"
        assert type(the_file_is_stereo) == bool

    audio = AudioSegment.from_mp3(file_name)
    vs__yL__int = None
    v2__y__int = np.array(audio.get_array_of_samples())

    if the_file_is_stereo:

        va__yL__idx = [i for i in range(len(v2__y__int)) if i % 2 == 0] # L
        vs__yL__int = v2__y__int[va__yL__idx]

        va__yR__idx = [i for i in range(len(v2__y__int)) if i % 2 == 1] # R
        vs__yR__int = v2__y__int[va__yR__idx]

    else: # mono audio

        vs__yL__int = v2__y__int

    return vs__yL__int



# ~~ 4.1.1. Constant function value vector calculation ~~~~~~~~~~~~~~~~~~~~~~~~~

def hann_window(l,
                L = 64
               ):
    if DEBUGGING:
        assert ( issubclass(type(l),   np.integer) or
                            type(l)    == int    )
        assert              type(L)    == int
        assert                   L % 2 == 0
        assert                   l     >= 0
        assert                   l     <= L
    
    coord = 2*np.pi*l / L
    out   = 0.5 - 0.5*np.cos(coord) 
    
    if DEBUGGING:
        assert     isinstance(     out ,  numbers.Number)
        assert not issubclass(type(out),  np.integer)
        assert                     out >= 0.0
        assert                     out <= 1.0
    
    return out


def prototype_function(l,
                       win_L,
                       L = 64,
                       M = 64
                      ):
    if DEBUGGING:
        assert ( issubclass(type(l),                np.integer) or
                            type(l)                 == int    )
        assert                   win_L.dtype        == np.dtype('float64')
        assert              type(L)                 == int
        assert                   L % 2              == 0
        assert               len(win_L)             == L + 1
        assert                   l                  >= 0
        assert                   l                  <= L
        assert              type(M)                 == int
        assert                   M % 2              == 0
    
    divisor = None
    sine    = None
    
    if l == L/2: # solved by l'Hopital's rule
        divisor = M
        sine    = 1.0
        
    else:
        divisor = 2 * np.pi * (l - L/2)
        sine    = np.sin( divisor / float(M) )
    
    window = win_L[l]
    out    = sine * window / divisor
    
    if DEBUGGING:
        assert     isinstance(     out , numbers.Number)
        assert not issubclass(type(out), np.integer)
    
    return out



# ~~ 4.1.2. Constant function value matrix calculation ~~~~~~~~~~~~~~~~~~~~~~~~~

def modulation_sequence(i,
                        l,
                        M = 64
                       ):
    if DEBUGGING:
        assert ( issubclass(type(i),   np.integer) or
                            type(i)    == int    )
        assert ( issubclass(type(l),   np.integer) or
                            type(l)    == int    )
        assert              type(M)    == int
        assert                   M % 2 == 0
        assert                   i     >= 0
        assert                   i     <= M

    jay   = np.complex128(1j)
    coord = - 2.0 * jay * np.pi * i * l / M
    out   = np.exp(coord)
    
    if DEBUGGING:
        assert type(out) == np.complex128
    
    return out



# ~~ 4.2.1. Variable function value vector / matrix calculation ~~~~~~~~~~~~~~~~

# ---- 4.2.1a. beta_now(l) -----------------------------------------------------
# see fbe_mid_level_functions.py

# ---- 4.2.1b. Y_i_now ---------------------------------------------------------
# see fbe_sunk_level_functions.py

# ---- 4.2.1c. Y_mat, Y_1, Y_S, Y_L --------------------------------------------
# see fbe_sunk_level_functions.py

# ---- 4.2.1d. P_i -------------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1e. E(P_i) ----------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1f. E(P_i^2) --------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1g. Var(P_i) ------------------------------------------------------
# see fbe_yawn_level_functions.py

# ---- 4.2.1h. W_i_now ---------------------------------------------------------
# see fbe_sunk_level_functions.py and fbe_yawn_level_functions.py

# ---- 4.2.1i. w_l_now ---------------------------------------------------------
# see fbe_sunk_level_functions.py



# ~~ 5. Analysis filter bank ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ---- 5a. Update the value of P_i, E(P_i), E(P_i^2) and Var(P_i) --------------
# see fbe_sunk_level_functions.py

# ---- 5b. Update the value of w_l^now for n^now -------------------------------
# see fbe_mid_level_functions.py



# ~~ 6. Principal function ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ---- 6a. Real-time plotting --------------------------------------------------
# see fbe_mid_level_functions.py

# ---- 6b. The main function ---------------------------------------------------

def main_function(y,
                  v,
                  monad_beta_l,
                  h_l,
                  monad_w_l,
                  monad_W_i,
                  monad_Y_i,
                  h_phi,
                  n_axis,
                  i_axis,
                  l_axis,
                  monad_n_now,
                  monad_Y_mat,
                  monad_Y_1,
                  monad_Y_S,
                  monad_Y_L,
                  monad_P_i,
                  monad_EP_i,
                  monad_EP2_i,
                  monad_varP_i,
                  i_cot_a_i_6,
                  HL_cot_a_i,
                  T_i_nml,
                  monad_W_ABS,
                  W_ABP,
                  D              = 0.99,
                  max_possible_dB_SPL            = 117.4,
                  M              = 64,
                  L              = 64,
                  R              = 441,
                  B_S            = 5,
                  B_L            = 200,
                  PLS_GIVE_PLOTS = True
                 ):
    
    if DEBUGGING:
        assert                type(y)                 == np.ndarray
        assert          isinstance(y[0],                 numbers.Number)
        assert                type(v)                 == np.ndarray
        assert                     v.dtype            == y.dtype
        assert                type(monad_beta_l)      == np.ndarray
        assert          isinstance(monad_beta_l[0],      numbers.Number)
        assert     isinstance(     h_l[0] ,              numbers.Number)
        assert not issubclass(type(h_l),                 np.integer)
        assert                type(monad_w_l)         == np.ndarray
        assert          isinstance(monad_w_l[0],         numbers.Number)
        assert not issubclass(type(monad_w_l),           np.integer)
        assert                type(monad_W_i)         == np.ndarray
        assert          isinstance(monad_W_i[0],         numbers.Number)
        assert not issubclass(type(monad_W_i),           np.integer)
        assert                type(monad_Y_i)         == np.ndarray
        assert                type(h_phi)             == np.ndarray
        assert                 len(h_phi.shape)       == 2
        assert     np.iscomplexobj(h_phi)
        assert                     monad_Y_i.dtype    == h_phi.dtype
        assert                type(i_axis)            == np.ndarray
        assert     issubclass(type(i_axis[0])          , np.integer)
        assert                type(l_axis)            == np.ndarray
        assert     issubclass(type(l_axis[0])          , np.integer)
        assert                type(n_axis)            == np.ndarray
        assert     issubclass(type(n_axis[0])          , np.integer)
        assert   ( issubclass(type(monad_n_now)        , np.integer) or
                              type(monad_n_now)       == int)
        assert                type(monad_Y_mat)       == np.ndarray
        assert                 len(monad_Y_mat.shape) == 2
        assert          isinstance(monad_Y_mat[0,0],     numbers.Number)
        assert not issubclass(type(monad_Y_mat),         np.integer)
        assert                type(monad_Y_1)         == np.ndarray
        assert          isinstance(monad_Y_1[0],         numbers.Number)
        assert not issubclass(type(monad_Y_1),           np.integer)
        assert                type(monad_Y_S)         == np.ndarray
        assert          isinstance(monad_Y_S[0],         numbers.Number)
        assert not issubclass(type(monad_Y_S),           np.integer)
        assert                type(monad_Y_L)         == np.ndarray
        assert          isinstance(monad_Y_L[0],         numbers.Number)
        assert not issubclass(type(monad_Y_L),           np.integer)
        assert                type(monad_P_i)         == np.ndarray
        assert          isinstance(monad_P_i[0],         numbers.Number)
        assert not issubclass(type(monad_P_i),           np.integer)
        assert                type(monad_EP_i)        == np.ndarray
        assert          isinstance(monad_EP_i[0],        numbers.Number)
        assert not issubclass(type(monad_EP_i),          np.integer)
        assert                type(monad_EP2_i)       == np.ndarray
        assert          isinstance(monad_EP2_i[0],       numbers.Number)
        assert not issubclass(type(monad_EP2_i),         np.integer)
        assert                type(monad_varP_i)      == np.ndarray
        assert          isinstance(monad_varP_i[0],      numbers.Number)
        assert not issubclass(type(monad_varP_i),        np.integer)
        assert                type(i_cot_a_i_6)       == np.ndarray
        assert          isinstance(i_cot_a_i_6[0],       numbers.Number)
        assert not issubclass(type(i_cot_a_i_6),         np.integer)
        assert                 len(i_cot_a_i_6)       == len(i_axis)
        assert                type(HL_cot_a_i)        == np.ndarray
        assert          isinstance(HL_cot_a_i[0],        numbers.Number)
        assert not issubclass(type(HL_cot_a_i),          np.integer)
        assert                 len(HL_cot_a_i)        == len(i_axis)
        assert                type(T_i_nml)           == np.ndarray
        assert          isinstance(T_i_nml[0],           numbers.Number)
        assert not issubclass(type(T_i_nml),             np.integer)
        assert                 len(T_i_nml)           == len(i_axis)
        assert                type(monad_W_ABS)       == np.ndarray
        assert          isinstance(monad_W_ABS[0],       numbers.Number)
        assert not issubclass(type(monad_W_ABS),         np.integer)
        assert                type(W_ABP)             == np.ndarray
        assert          isinstance(W_ABP[0],             numbers.Number)
        assert not issubclass(type(W_ABP),               np.integer)
        assert                type(D)                 == float
        assert                type(max_possible_dB_SPL)               == float
        assert                     h_phi.shape[0]     == len(i_axis)
        assert                     h_phi.shape[1]     == len(l_axis)
        assert                 len(y)                 == len(n_axis)
        assert                 len(v)                 == len(n_axis)
        assert                 len(monad_beta_l)      == len(l_axis)
        assert                 len(h_l)               == len(l_axis)
        assert                 len(monad_w_l)         == len(l_axis)
        assert                 len(monad_W_i)         == len(i_axis)
        assert                 len(monad_Y_i)         == len(i_axis)
        assert                type(M)                 == int
        assert                     M % 2              == 0
        assert                     M                  == L
        assert                type(R)                 == int
        assert                     R                   > 0
        assert                type(B_S)               == int
        assert                type(B_L)               == int
        assert                     B_S                 < B_L
        assert                 len(monad_Y_mat[:,0])  == len(i_axis)
        assert                 len(monad_Y_mat[0,:])  == B_L
        assert                 len(monad_Y_1)         == len(i_axis)
        assert                 len(monad_Y_S)         == len(i_axis)
        assert                 len(monad_Y_L)         == len(i_axis)
        assert                 len(monad_P_i)         == len(monad_Y_1)
        assert                 len(monad_P_i)         == len(monad_Y_S)
        assert                 len(monad_P_i)         == len(monad_EP_i)
        assert                 len(monad_EP_i)        == len(monad_Y_L)
        assert                 len(monad_EP_i)        == len(monad_EP2_i)
        assert                 len(monad_varP_i)      == len(monad_EP_i)
        assert                 len(monad_W_ABS)       == len(i_axis)
        assert                 len(W_ABP)             == len(i_axis)
        assert                type(PLS_GIVE_PLOTS)    == bool
    
    mgr = mp.Manager()
    
    # 1. Prepare and start AFB thread -  -  -  -  -  -  -  -  -  -  -  -  -  -  
    
    pls_stop_FB  = threading.Event()
    boxed_n_now  = [monad_n_now]
    boxed_beta_l = [np.array(monad_beta_l)] # create copies of these arrays so
    boxed_Y_i    = [np.array(monad_Y_i)]    # there's no risk of the values of 
    boxed_W_i    = [np.array(monad_W_i)]    # the arrays changing monadside as 
    boxed_w_l    = [np.array(monad_w_l)]    # it's using them for something 
    boxed_Y_mat  = [np.array(monad_Y_mat)]
    boxed_Y_1    = [np.array(monad_Y_1)]
    boxed_Y_S    = [np.array(monad_Y_S)]
    boxed_Y_L    = [np.array(monad_Y_L)]
    boxed_P_i    = [np.array(monad_P_i)]
    boxed_EP_i   = [np.array(monad_EP_i)]
    boxed_EP2_i  = [np.array(monad_EP2_i)]
    boxed_varP_i = [np.array(monad_varP_i)]
    boxed_W_ABS  = [np.array(monad_W_ABS)]
    
    pls_stop_plt = mp.Event()
    multi_W_i    = mgr.list(boxed_W_i[0].tolist())
    
    h_w = h_l * boxed_w_l[0]
    
    the_AFB = threading.Thread( target = AFB_sidechain,
                                args   = ( pls_stop_FB,
                                           boxed_n_now,
                                           y,
                                           h_phi,
                                           boxed_beta_l,
                                           boxed_Y_i,
                                           boxed_W_i,
                                           boxed_w_l,
                                           i_axis,
                                           l_axis,
                                           boxed_Y_mat,
                                           boxed_Y_1,
                                           boxed_Y_S,
                                           boxed_Y_L,
                                           boxed_P_i,
                                           boxed_EP_i,
                                           boxed_EP2_i,
                                           boxed_varP_i,
                                           i_cot_a_i_6,
                                           HL_cot_a_i,
                                           T_i_nml,
                                           boxed_W_ABS,
                                           W_ABP,
                                           D,
                                           max_possible_dB_SPL,
                                           M,
                                           L,
                                           B_S,
                                           B_L
                                         )
                              )
    
    the_plt = None
    if PLS_GIVE_PLOTS:
        the_plt = mp.Process( target = RT_plot,
                              args   = ( pls_stop_plt,
                                         i_axis,
                                         multi_W_i
                                       )
                            )
    if PLS_GIVE_PLOTS:
        the_plt.start()
    
    the_AFB.start()
    
    # 2. Parse input audio (main chain + AFB sidechain) -  -  -  -  -  -  -  
    
    tic = time.perf_counter()
    
    for monad_n_now in n_axis:
        
        if DEBUGGING:
            assert the_AFB.is_alive()
            if monad_n_now % int(len(v) / 100) == 0:
                print(round(100 * monad_n_now / len(v)), 
                      '%', 
                      end="\r", 
                      flush = True
                     )
        
        monad_beta_l = get_beta_now(y,
                                    monad_n_now,
                                    l_axis
                                   )
        if monad_n_now % R == 0:
            
            boxed_n_now[0]  = monad_n_now
            boxed_beta_l[0] = np.array(monad_beta_l) # this passes to the 
                                                     # child a copy of the
                                                     # current value of beta_l.
                                                     # This way if the child 
                                                     # takes too long to 
                                                     # calculate w_l, at least
                                                     # beta_l won't change 
                                                     # during the calculation

            h_w = h_l * boxed_w_l[0] # not creating a copy here because at 
                                     # worst we'll have 100 wonky samples
                                     # per second. That's a loss I can accept
                    
            if PLS_GIVE_PLOTS:
                assert the_plt.is_alive()
                multi_W_i[:] = boxed_W_i[0][:]
            
        v[monad_n_now] = np.dot(monad_beta_l, h_w).astype(y.dtype)
    
    toc = time.perf_counter()
    duration = toc - tic
    
    # 3. Close AFB thread -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
    
    if PLS_GIVE_PLOTS:
        pls_stop_plt.set()
        while the_plt.is_alive():
            time.sleep(0.1)
        the_plt.join()
    
    pls_stop_FB.set()
    while the_AFB.is_alive():
        time.sleep(0.1)
    the_AFB.join()
    
    if DEBUGGING:
        assert type(v)      == np.ndarray
        assert      v.dtype == y.dtype
        assert  len(v)      == len(n_axis)
        print("done! ÔvÔ", end="\r", flush = True)
        print("duration =", duration, " s")

    return v
