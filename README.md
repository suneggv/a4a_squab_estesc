# A4A SQUAB ESTeSC
![PRRUU](prruu.png)

The PRRUU audio processing block, outside of the server. Based on the filter bank equalizer by Löllman e Vary **[1]**. Frequency shaping based on the recruitment-based compensation by Ramani **[2]**.

* **[1]** - H. W. Löllman, P. Vary - *Generalized filter-bank equalizer for noise reduction with reduced signal delay* - Proc. Interspeech 2005, 2105-2108 *(`DOI: 10.21437/Interspeech.2005-686`)*
* **[2]** - M. Ramani - *Noise Robust Algorithms to Improve Cell Phone Speech Intelligibility for the Hearing Impaired* - PhD thesis, University of Florida, 2008 *(`ISBN:978-1-124-12234-2`)*

## Requirements
* portaudio 19.6.0-1.2 or later _(available on ubuntu 22.10)_
* pyaudio
* pydub
* numpy
* matplotlib *(optional)*

## Running

### Running SQUAB ESTeSC
SQUAB expects stereo or mono mp3 or wav files normalized so that the maximum amplitude of the waveform is the maximum possible value allowed by the bitrate. In this state, by default, the sound level of an input file is taken to be 117.4 dB SPL. Store a collection of input files in `./vox_raw/Dissilabos` and set a desired input dB SPL. SQUAB ESTeSC will output processed versions of the files to `./vox_FBE/estesc_audiogram`. The result is stereo output files adjusted so that a user with the given input audiogram will hear them the same way as would a normal hearing person hear the original input file.

* **Basic usage**
    * Place input files in `./vox_raw/Dissilabos`
    * `$ python FBE_fullScript.py`
    * It will ask:
        * `Device ('PC -> MDREX' (default) | '2i4 -> PELTOR' | smth else)?`
        * *This step is asking what kind of audio interfaces you're using. It's used only for setting the expected maximum possible dB SPL of an input audio and expected peak power in mW. You can ignore it if you want, and it will assume you're using small earphones connected to the PC jack. The only consequence is that the output audio may turn out too quiet or loud at the end.*
    * Output files will show up in `./vox_FBE/estesc_audiogram`
* **Changing input directory**
    * In `FBE_fullScript.py`, go to line 54, change the value of `directory_in` to the desired one.
* **Changing output directory**
    * In `FBE_fullScript.py`, go to line 56, change the value of `directory_out` to the desired one.
* **Setting desired input dB SPL**
    * In `FBE_fullScript.py`, go to line 50, change the value of `goal_rms_dB_SPL` to the desired one.
* **Adjusting maximum sound level to the currently used device**
    * When the script asks for the `Device`, answer something other than nothing, `PC -> MDREX`, or `2i4 -> PELTOR`. You will then be requested to tell your setup's max possible dB SPL and peak power in mW.
* **Adjusting audiogram**
    * In `./freqShapDat`, edit the `A_k_L.txt` and `A_k_R.txt` files: they represent the audiogram for the left and right ear, respectively. SQUAB expects eight positive auditory thresholds, all in a single row, separated by tabs. The frequencies of the auditory thresholds are 0.25, 0.5, 1, 2, 3, 4, 6 and 8 kHz.
