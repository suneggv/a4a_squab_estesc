# ==============================================================================
# ==  2.1 IMPORTS  =============================================================
# ==============================================================================

# ~~ module access from main ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import sys
sys.path.append('./FBEmodules')

# -- audio manipulation --------------------------------------------------------
import numpy as np
import math
from pydub import AudioSegment


# -- record / playback ---------------------------------------------------------
import pyaudio
import time


# -- DCT -----------------------------------------------------------------------
import scipy as sp


# -- sidechain -----------------------------------------------------------------
import threading


# -- visualization -------------------------------------------------------------
from debugging_and_visualization import VISUALIZATION
import matplotlib.pyplot as plt 
import scipy.signal as signal
from   scipy.fft import fft, fftfreq, fftshift, dct
import multiprocessing as mp

# -- debugging -----------------------------------------------------------------
from debugging_and_visualization import DEBUGGING
import numbers


# -- file navigation -----------------------------------------------------------
import os


# ==============================================================================
# ==  3.1.1 PARAMETERS  ========================================================
# ==============================================================================

# -- desired input dB SPL ------------------------------------------------------

goal_rms_dB_SPL = 105.0 # this amplitude is good

# -- file addresses ------------------------------------------------------------

directory_in  = './vox_raw/Dissilabos/'
directory_dB  = './vox_adjust/estesc_audiogram/'
directory_out = './vox_FBE/estesc_audiogram/'

filename_in    = "145approach2_quiet.mp3"
filename_out   = "encapsul_test.mp3"
# ~ addr__file_name    = "177allIsBusy2_quiet.mp3"
the_file_is_stereo = True

addr__A_k_L   = './freqShapDat/A_k_L.txt'
addr__A_k_R   = './freqShapDat/A_k_R.txt'
addr__T_i_nml = './freqShapDat/T_i_nml.txt'
addr__C_ik    = './freqShapDat/C_ik.txt'
addr__W_ABP   = './ABPdat/W_ABP.txt'


# -- pyaudio -------------------------------------------------------------------

pm__chunk__sam         = 1024            # Each chunk will be 1024 samples long
pm__sample_format__int = pyaudio.paInt16 # 16 bits per sample
pm__channels__int      = 2               # Number of audio channels
pm__fs__Hz             = 44100           # Record at 44100 samples per second


# -- FBE -----------------------------------------------------------------------

pm__M__int = 64  # number of channels
pm__L__int = 64  # length of filter
pm__R__sam = 441 # refresh samples 


# -- level detection -----------------------------------------------------------

pm__BS__int = 5   # short backlog (ie # of past Yi values to use for lvl detect)
pm__BL__int = 200 # long  backlog (ie # of past Yi values to use for statistics)


# -- adaptive band subtraction -------------------------------------------------

pm__decay__fct = 0.99


# -- frequency shaping ---------------------------------------------------------

device = input("Device ('PC -> MDREX' (default) | '2i4 -> PELTOR' | smth else)?\n\t")
if len(device) == 0:
    print("Setting device = 'PC -> MDREX'")
    device = 'PC -> MDREX'

max_possible_dB_SPL, P_pk_mW, P_0_mW = None, None, None
match device:
    case 'PC -> MDREX':
        max_possible_dB_SPL = 120.4     # PC -> MDREX
        P_pk_mW             = 55.0      # PC -> MDREX
        P_0_mW = P_pk_mW * math.pow(10.0, -0.1*max_possible_dB_SPL)
    case '2i4 -> PELTOR':
        max_possible_dB_SPL = 90.0       # 2i4 -> PELTOR
        P_pk_mW             = 6.6        # 2i4 -> PELTOR
        P_0_mW              = 3.7542e-09 # 2i4 -> PELTOR
    case _:
        max_possible_dB_SPL = float(input("    Max possible dB SPL ? "))
        P_pk_mW             = float(input("    Peak power (mW)     ? "))
        P_0_mW = P_pk_mW * math.pow(10.0, -0.1*max_possible_dB_SPL)

u_factor       = math.sqrt((2.0*P_0_mW)/P_pk_mW)
max_rms_dB_SPL = 10 * math.log10(P_pk_mW/(2.0*P_0_mW))

print(f"Device details:")
print(f"               peak dB SPL = {max_possible_dB_SPL} dB SPL")
print(f"               RMS  dB SPL = {max_rms_dB_SPL} dB SPL")
print(f"               peak power  = {P_pk_mW} mW")
print(f"               convert fct = {P_0_mW} mW")
print(f"               amplit. fct = {u_factor}")

# -- limiting ------------------------------------------------------------------

want_limiter = False

match device:
    case 'PC -> MDREX':
        bige         = 3000
        a            = 4000
    case '2i4 -> PELTOR':
        bige         = 30000
        a            = 4000
    case _:
        bige         = 15000
        a            = 4000

# -- matplotlib.pyplot ---------------------------------------------------------

plt.rcParams["figure.figsize"] = (9,3)



# ==============================================================================
# ==  FUNCTION IMPORTS  ========================================================
# ==============================================================================

# STUFF GOES HERE

# == imports -- encapsul =======================================================

from FBEfilSte_encapsul import brick_h
from FBEfilSte_encapsul import brick_j
from FBEfilSte_encapsul import brickwall

from FBEfilSte_encapsul import FBE_with_files

from FBEfilSte_encapsul import FBE_runner


# == imports -- high ===========================================================

# -- _______ -- ___ -- Import audio -  -  -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import import_audio # y(n)

# -- _______ -- ___ -- Constant FVV calculation -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import hann_window        # win_L(l)
from fbe_high_level_functions import prototype_function # h(l)

# -- _______ -- ___ -- Constant FVM calculation -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import modulation_sequence # varphi(i,l)

# -- _______ -- ___ -- Principal function -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_high_level_functions import main_function # v(n)


# == imports -- mid ============================================================

# -- _______ -- ___ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import get_beta_now # beta_now(l)

# -- _______ -- ____ -- Analysis filter bank -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import AFB_sidechain # w_l^now for n^now

# -- _______ -- ____ -- Principal function -  -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_mid_level_functions import RT_plot # Real-time plotting


# == imports -- sunk ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import get_Y_now   # Y_i_now
from fbe_sunk_level_functions import update_Ymat # Y_mat, Y_1, Y_S, Y_L
from fbe_sunk_level_functions import get_W_now   # W_i_now
from fbe_sunk_level_functions import get_w_now   # w_l_now

# -- _______ -- ____ -- Analysis filter bank -  -  -  -  -  -  -  -  -  -  -  - 
from fbe_sunk_level_functions import update_stats # P_i,E(P_i),E(P_i^2),Var(P_i)


# == imports -- yawn ===========================================================

# -- _______ -- ____ -- variable FVV/M calculation -  -  -  -  -  -  -  -  -  - 
from fbe_yawn_level_functions import get_P               # P_i
from fbe_yawn_level_functions import get_avgP            # E(P_i)
from fbe_yawn_level_functions import get_avgP2           # E(P_i^2)
from fbe_yawn_level_functions import get_varP            # Var(P_i)
from fbe_yawn_level_functions import spect_lin_to_dB_SPL # W_i_now
from fbe_yawn_level_functions import get_W_ABS           # W_i_now
from fbe_yawn_level_functions import get_W_FSh           # W_i_now

# == imports -- debug ==========================================================

from debug_funcs import PrintException



# ==============================================================================
# ==  FUNCTIONS  ===============================================================
# ==============================================================================


def normalize_audio( y,
                     sample_format
                   ):
    if DEBUGGING:
        assert type(y)             == np.ndarray
        assert type(sample_format) == int

    bitrate  = 8 * pyaudio.get_sample_size(sample_format)
    max_val  = math.pow(2, bitrate - 1 )
    y_f      = y.astype('float128')
    y_f_1    = y_f / np.max(y_f)
    y_f_full = (y_f_1 * max_val).astype(y.dtype)

    if DEBUGGING:
        assert type(y_f_full)      == np.ndarray
        assert      y_f_full.dtype == y.dtype
    return y_f_full


def adjust_dB_SPL( y,
                   goal_rms_dB_SPL,
                   max_rms_dB_SPL,
                   u_factor
                 ):
    if DEBUGGING:
        assert type(y)               == np.ndarray
        assert type(goal_rms_dB_SPL) == float
        assert type(max_rms_dB_SPL)  == float
        assert goal_rms_dB_SPL       <= max_rms_dB_SPL
        assert type(u_factor)        == float

    dB_factor = math.pow(10.0 , 0.05*goal_rms_dB_SPL)
    y_f       = y.astype('float128')
    out       = (u_factor * dB_factor * y_f).astype(y.dtype)

    if DEBUGGING:
        assert type(out)      == np.ndarray
        assert  len(out)      == len(y)
        assert      out.dtype == y.dtype

    return out


def adjust_file_dB( filename_in,
                    filename_dB,
                    goal_rms_dB_SPL,
                    max_rms_dB_SPL,
                    u_factor,
                    sample_format,
                    fs,
                  ):
    if DEBUGGING:
        assert type(filename_in)    == str
        assert type(filename_dB)   == str
        assert type(goal_rms_dB_SPL) == float
        assert type(max_rms_dB_SPL)  == float
        assert goal_rms_dB_SPL       <= max_rms_dB_SPL
        assert type(u_factor)        == float
        assert type(sample_format)   == int

    audio = None
    if filename_in[-4:] == ".wav":
        audio = AudioSegment.from_wav(filename_in)
    elif filename_in[-4:] == ".mp3":
        audio = AudioSegment.from_mp3(filename_in)
    else:
        raise NotImplementedError("wait a little pls :c")

    y      = np.array(audio.get_array_of_samples())
    y_norm = normalize_audio( y, sample_format)
    y_a_dB = adjust_dB_SPL( y_norm,
                            goal_rms_dB_SPL,
                            max_rms_dB_SPL,
                            u_factor
                          )
    output_audio = AudioSegment( y_a_dB.tobytes(),
                                 frame_rate   = audio.frame_rate,
                                 sample_width = y_a_dB.dtype.itemsize,
                                 channels     = audio.channels
                               )
    output_audio.export( filename_dB, 
                         format  = "mp3", 
                         bitrate = "128k"
                       )




# ==============================================================================
# ==  SCRIPT  ==================================================================
# ==============================================================================

print("Directory: ", directory_in)
for file_in_raw in os.listdir(directory_in):

    print(f"Current file: {file_in_raw}")
    filename_in  = directory_in + file_in_raw
    filename_dB  = directory_dB + file_in_raw[:-4] + '_'  + \
                   str(int(goal_rms_dB_SPL)) + '.mp3'
    filename_out = directory_out + file_in_raw[:-4] + '_' + \
                   str(int(goal_rms_dB_SPL)) + 'FBE.mp3'

    print(f"adjusting {filename_in} for {goal_rms_dB_SPL} dB SPL output")
    adjust_file_dB( filename_in     = filename_in,
                    filename_dB     = filename_dB,
                    goal_rms_dB_SPL = goal_rms_dB_SPL,
                    max_rms_dB_SPL  = max_rms_dB_SPL,
                    u_factor        = u_factor,
                    sample_format   = pm__sample_format__int,
                    fs              = pm__fs__Hz,
                  )
    print(f"dB-adjusted file: {filename_dB}")

    print("Running FBE")
    _ = FBE_runner( filename_in            = filename_dB,
                    filename_out           = filename_out,
                    addr__A_k_L            = addr__A_k_L,
                    addr__A_k_R            = addr__A_k_R,
                    addr__T_i_nml          = addr__T_i_nml,
                    addr__C_ik             = addr__C_ik,
                    addr__W_ABP            = addr__W_ABP,
                    pm__chunk__sam         = pm__chunk__sam,
                    pm__sample_format__int = pm__sample_format__int,
                    pm__channels__int      = pm__channels__int,
                    pm__fs__Hz             = pm__fs__Hz,
                    pm__M__int             = pm__M__int,
                    pm__L__int             = pm__L__int,
                    pm__R__sam             = pm__R__sam,
                    pm__BS__int            = pm__BS__int,
                    pm__BL__int            = pm__BL__int,
                    pm__decay__fct         = pm__decay__fct,
                    max_possible_dB_SPL    = max_possible_dB_SPL,
                    want_limiter           = want_limiter,
                    bige                   = bige,
                    a                      = a,
                    the_file_is_stereo     = the_file_is_stereo
                  )
